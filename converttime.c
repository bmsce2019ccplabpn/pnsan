#include<stdio.h>

void input(int*hr, int*min)
{
    int x, y;
    printf("\n Enter the hours: ");
    scanf("%d", &x);
    printf("\n Enter the minutes: ");
    scanf("%d", &y);
    *hr=x;
    *min=y;
}

int convert(int*hr, int*min)
{
    int time=*hr*60+*min;
    return time;
}

void output(int*hr, int*min, int*time)
{
    printf("\n The time %d:%d in minutes is = %d minutes.\n\n",*hr, *min, time);
}

int main()
{
    int hr, min;
    input(&hr, &min);
    int time=convert(&hr, &min);
    output(&hr, &min, time);
    return 0;
}